package com.devcamp.midtest1.s20.s20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S20Application {

	public static void main(String[] args) {
		SpringApplication.run(S20Application.class, args);
	}

}
