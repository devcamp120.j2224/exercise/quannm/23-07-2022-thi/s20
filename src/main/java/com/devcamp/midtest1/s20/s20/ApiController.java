package com.devcamp.midtest1.s20.s20;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/reverse-string")
    public String getReverse(@RequestParam(required = true, name = "inpStr") String inp) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = inp.toCharArray();
        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }
        return strBuilder.toString();
    }

    @CrossOrigin
    @GetMapping("/check-palindrome")
    public String getCheckPalindrome(@RequestParam(required = true, name = "inpStr") String inp) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = inp.toCharArray();
        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }
        String str = strBuilder.toString();
        if (str.equals(inp)) {
            return "Đây là chuỗi Palindrome";
        }
        else {
            return "Đây không là chuỗi Palindrome";
        }
    }

    @CrossOrigin
    @GetMapping("/remove-charDup")
    public String getRemoveCharDup(@RequestParam(required = true, name = "inpStr") String inp) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < inp.length(); i++) {
            if (!charsPresent.contains(inp.charAt(i))) {
                stringBuilder.append(inp.charAt(i));
                charsPresent.add(inp.charAt(i));
            }
        }
        return stringBuilder.toString();
    }

    @CrossOrigin
    @GetMapping("/append-string")
    public String getAppendString(@RequestParam String inpStr1, String inpStr2) {
        if (inpStr1.length() == inpStr2.length())
            return inpStr1 + inpStr2;
        if (inpStr1.length() > inpStr2.length())
        {
            int diff = inpStr1.length() - inpStr2.length();
            return inpStr1.substring(diff, inpStr1.length()) + inpStr2;
        } else
        {
            int diff = inpStr2.length() - inpStr1.length();
            return inpStr1 + inpStr2.substring(diff, inpStr2.length());
        }

    }
}
